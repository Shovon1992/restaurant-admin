import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import {Icreatwelcome } from '../../../Interface/Restaurant/icreatwelcome';
import { Storage } from 'aws-amplify';
import { ICreateCatagory } from 'src/app/Interface/Food/icreate-catagory';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-food-menu',
  templateUrl: './food-menu.component.html',
  styleUrls: ['./food-menu.component.scss']
})
export class FoodMenuComponent extends BaseComponent implements OnInit {
/**
   * Variables 
   */
  restaurantData = this.dataService.getData();
  
  tempImage: any = '../../../assets/img/noImage.jpg'
  // showOverlay = true
  createCatagoryData : ICreateCatagory = {
    resturantId : this.restaurantData.hash_id,
    description: '',
    imageUrl: '',
    name: '',
    thumbUrl: ''
    
  }
  ngOnInit(){
    console.log(this.restaurantData.hash_id)
    this.getCatagoryList();
  }

 

  /**
   * Create new restaurant 
   */
  createCatagory(){
    if(this.createCatagoryData.description && this.createCatagoryData.name){
      this.spinner.show();
      this.loaddingMessage = 'Creating Restaurant'
      this.foodService.createFoodMenues(this.createCatagoryData).subscribe(
        res =>{
          this.spinner.hide()
          if(res.messageCode == 200){
            console.log(document.getElementById('modalColoseBtn'));
            document.getElementById('modalColoseBtn').click();
            this.getCatagoryList();

          }
          
          console.log(res);

        }, err =>{
          console.log(err)
        }
      );
    } else{
      alert('All Fields are mandatory..')
    }
    
  }

  /**
   * Go to restuarant dashboard page 
   */
  goRestaurantDashboard(restaurantData){
    this.dataService.setData(restaurantData);
    this.router.navigateByUrl('dashboard/restaurant-dashboard');
  }

 /**
   * Upload image to s3
   */

//   uploadImage(image,imageType){
//     console.log(image,imageType)
//     Storage.put('restaurant/menuList/'+image[0].name,image[0],{
//       contentType: 'image/jpg',
//       bucket: environment.catagory,
//       progressCallback(progress) {
//         console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
//   }
// }).then (
//   res => {
//     this.createCatagoryData.imageUrl = 'https://'+ environment.catagory +'.s3.amazonaws.com/public/'+res['key']
//     this.createCatagoryData.thumbUrl = 'https://'+ environment.catagoryThumb +'.s3.amazonaws.com/public/'+res['key']
//   }
// )
//     .catch(err => console.log(err));
//   }



  uploadImage(image,imageType){
    console.log(image,imageType)
    this.reader.readAsDataURL(image[0]);
    
    this.reader.onload = () =>{
      console.log(this.reader.result);
      this.tempImage = this.reader.result;
    }
    
    //this.sanitizer.bypassSecurityTrustResourceUrl(image.target.value);
    console.log(this.tempImage);
    this.showOverlay = true;
  //   Amplify.configure({
  //     Storage: {
  //         AWSS3: {
  //             bucket: 'menume-dev-restaurant-doc',
  //         }
  //     }
  // });
  
  Storage.configure
    Storage.put('image/'+this.restaurantData.name+'/'+image[0].name,image[0],{
          bucket: environment.catagory,
          contentType: 'image/jpg',
          progressCallback(progress) {
            console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
          },
          
    }).then (
      res => {
        console.log(res);
        this.createCatagoryData.imageUrl = 'https://'+ environment.catagory +'.s3.amazonaws.com/public/'+res['key']
        this.createCatagoryData.thumbUrl = 'https://'+ environment.catagory +'.s3.amazonaws.com/public/'+res['key']
        this.showOverlay = false;
      }
    ).catch(err => {
      console.log(err); 
      this.showOverlay = false;
    });
    
  }
  /**
   * End of classs
   */

}
