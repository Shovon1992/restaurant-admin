import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { ICreateItem } from '../../../Interface/Food/icreate-item';
import { Storage } from 'aws-amplify';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-food-items',
  templateUrl: './food-items.component.html',
  styleUrls: ['./food-items.component.scss']
})
export class FoodItemsComponent extends BaseComponent implements OnInit {
  restaurantData = this.dataService.getData();
  createFoodItemData : ICreateItem ={ 
    name : '',
    imageUrl : '',
    description : '',
    veg : 'Veg',
    price: 0.00,
    quantity : 1,
    unit: 'Plate',
    foodCategoryId : '',
    imageType: 1,
    thumbUrl: ''
  };
  foodItemList =[];
  ngOnInit() {
    this.getFoodItems();
    if(this.catagoryList.length < 1 ){
      this.getCatagoryList();
    }
  }


  /**
   * Create food item
   */
  createItem(){
    console.log(this.createFoodItemData)
    if(this.createFoodItemData.name && this.createFoodItemData.description && this.createFoodItemData.price && this.createFoodItemData.veg && this.createFoodItemData.unit && this.createFoodItemData.foodCategoryId){
    this.spinner.show();
    this.loaddingMessage = 'Creating Restaurant'
    try {
      this.foodService.createFoodItems(this.createFoodItemData).subscribe(
        res =>{
          this.spinner.hide()
          if(res.messageCode == 200){
            console.log(document.getElementById('modalColoseBtn'));
            document.getElementById('modalColoseBtn').click();
            this.getFoodItems();
  
          }
          
          console.log(res);
  
        }, err =>{
          this.spinner.hide()
          console.log(err)
        }
      );
    } catch (error) {
      
    }
    
  } else{
    alert('All Fields are mandatory..')
  }
  
}


/**
 * get food item lists
 */
getFoodItems(){
  console.log('food items',this.restaurantData)
  this.spinner.show();
    this.loaddingMessage = 'Creating Restaurant'
    try {
      this.foodService.getAllFoodItemsOfRestaurant(this.restaurantData.hash_id).subscribe(
        res =>{
          this.spinner.hide();
          console.log(res,res.messageCode)
          if(res.messageCode == 200){
            console.log(res.data);
            document.getElementById('modalColoseBtn').click();
            this.foodItemList = res.data;
  
          }
          
          console.log(res);
  
        }, err =>{
          this.spinner.hide()
          console.log(err)
        }
      );
    } catch (error) {
      console.log(error)
    }
    
  }
  

  uploadImage(image,imageType){
    console.log(image,imageType)
    this.reader.readAsDataURL(image[0]);
    
    this.reader.onload = () =>{
      console.log(this.reader.result);
      this.tempImage = this.reader.result;
    }
    
    //this.sanitizer.bypassSecurityTrustResourceUrl(image.target.value);
    console.log(this.tempImage);
    this.showOverlay = true;
  //   Amplify.configure({
  //     Storage: {
  //         AWSS3: {
  //             bucket: 'menume-dev-restaurant-doc',
  //         }
  //     }
  // });
  
  Storage.configure
    Storage.put('image/'+this.restaurantData.name+'/'+image[0].name,image[0],{
          bucket: environment.foodItem,
          contentType: 'image/jpg',
          progressCallback(progress) {
            console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
          },
          
    }).then (
      res => {
        console.log(res);
        this.createFoodItemData.imageUrl = 'https://'+ environment.foodItem +'.s3.amazonaws.com/public/'+res['key']
        this.createFoodItemData.thumbUrl = 'https://'+ environment.foodItem +'.s3.amazonaws.com/public/'+res['key']
        this.showOverlay = false;
      }
    ).catch(err => {
      console.log(err); 
      this.showOverlay = false;
    });
    
  }
  /**
   * End of the class 
   */
}
