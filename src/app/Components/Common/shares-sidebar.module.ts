import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from '../Common/headerSidebar/header/header.component'
import { SidebarComponent } from '../Common/headerSidebar/sidebar/sidebar.component'
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports : [
    HeaderComponent,
    SidebarComponent,
  ]
})
export class SharesSidebarModule { }
