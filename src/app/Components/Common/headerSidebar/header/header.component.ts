import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/Components/base/base.component';
import { Auth } from 'aws-amplify';
import { INotification } from 'src/app/Interface/notification';
// import { NgRedux, select } from '@angular-redux/store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseComponent implements OnInit {

  restaurantData = {
        cognito_id : "8e8ec954-9e63-4a87-b843-08fb778ddc55",
        name : "Test Resturant",
        email : "shov.mondal92@gmail.com",
        phone : "9476132302",
        avater : "https://static.toiimg.com/thumb/imgsize-191824,msid-79228089,width-400,resizemode-4/79228089.jpg",
        hash_id :"4eccd8fb9055ce1b1245fb948317208760591b8f"
    };
  totalNotification = 0;
  messageList = [];
  
  showMessage = false;
  message: INotification = {
    message: null,
    title: null,
    date: null
}
  ngOnInit(): void {
    // console.log("new message received. ", this.messagingService.currentMessage );
    this.messagingService.currentMessage.subscribe(res => {
      console.log(res)


      if (res != null) {
        this.showMessage = true;
        this.totalNotification++;
        
  this.message = {
    message: res["notification"]['body'],
    title: res["notification"]['title'],
    date: new Date()
  }
          this.messageList.push(this.message)
          setTimeout(() => {
           this.showMessage = false;
        }, 15000)
      }
     
    })
  }

/**
 * LOGOUT
 */
logout(){
  Auth.signOut({global: false}).then(
    res =>{
      console.log(res);
      this.router.navigateByUrl('login')
    },
    err =>{
      console.log(err)
    }
  ).catch(error =>{
    console.log(error)
  });

}
  /**
   * End of class
   */
}
