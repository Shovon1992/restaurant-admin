import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderSidebarComponent } from './header-sidebar.component';
import {BaseComponent} from '../base/base.component';
import {HomeComponent} from '../Dashboard/home/home.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SharesSidebarModule } from '../Common/shares-sidebar.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    HeaderSidebarComponent,
    BaseComponent,
    HomeComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    NgxSpinnerModule,
    FormsModule,
    BrowserAnimationsModule,
   SharesSidebarModule
  ]
})
export class HeaderSidebarModule { }
