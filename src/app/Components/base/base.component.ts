import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResturantService } from '.././../Services/Resturant/resturant.service';
import {DataService} from '../../Services/data.service';
import  { FoodService } from '../../Services/Food/food.service';
import { NgxSpinnerService } from "ngx-spinner";
import {NgRedux,select} from '@angular-redux/store';
import {IAppState} from '../../store';
import {SetUserName,BrowserInfo} from '../../action';
import { DomSanitizer } from '@angular/platform-browser';
import { OrderService } from '../../Services/Order/order.service';
import { FirebaseMessageService } from 'src/app/Services/Notification/firebase-message.service';
// Firebase App (the core Firebase SDK) is always required and must be listed first

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {


  /**Common variables */
  loaddingMessage : String = 'Loadding';
  static routingData : any;
  reader = new FileReader();
  tempImage: any = '../../assets/img/noImage.jpg';
  showOverlay = false; /** For showing overlay loader on image upload */
  restaurantData = this.dataService.getData();
  catagoryList: any =[]; /** For food menu catagory */
  constructor(
    public activatedRoute : ActivatedRoute,
    public router : Router,

    /**
     * Attributes
     */
    public spinner: NgxSpinnerService,
 
    /**
     * Service Lists
     */
      public dataService : DataService,
      public restaurantService : ResturantService,
      public foodService : FoodService,
      public oredrService : OrderService,
    public sanitizer: DomSanitizer,
      public messagingService : FirebaseMessageService,
    /**
     * redux
     */
    public ngRedux : NgRedux<IAppState>
  ) { }

  ngOnInit(): void {
  }
  /**
   * Stringify json data 
   */
  stringifyData(data){
    return JSON.stringify(data)
  }


  /**
   * Go to confirmation
   */
  confirmOTP(username){
    this.ngRedux.dispatch({type: SetUserName, userName: username});
    this.router.navigate(['confirmOTP']);
  }

 /**
   * Get resturant lists
   */
  getCatagoryList(){
    this.spinner.show();
    this.loaddingMessage = 'Loading Restaurant lists'
    this.foodService.getAllFoodMenues(this.restaurantData.hash_id).subscribe(
      res =>{
        this.spinner.hide()
        if(res.messageCode == 200){
          this.catagoryList = res.data
        }
        
        console.log(res);

      }, err =>{
        console.log(err)
      }
    );
  }
 


 
  /**
   * Get back to pervious screen
   */
  /**
   * End of class
   */
}
