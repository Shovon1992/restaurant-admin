import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { Auth } from 'aws-amplify';
import { Icreaterestaurant } from 'src/app/Interface/Restaurant/icreaterestaurant';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends BaseComponent implements OnInit {
  spinner = new NgxSpinnerService();
  loginData: Icreaterestaurant = {
    name: '', 
    email: '',
    phone: '',
    tagLine: '',
    reg_no: ''
  };
  password: String
  cpassword: String
  ngOnInit(): void {
  }

  /**
   * Complete register
   */
  register(username,password){
    // let username = this.loginData.email;
    this.spinner.show();
    if(this.loginData){
      Auth.signUp({
        username,
        password,
        attributes: {
            name: this.loginData.name,
            email: this.loginData.email,          // optional
          phone_number: '+91' + this.loginData.phone,
            'custom:reg_no': this.loginData.reg_no,
            'custom:tagLine' : this.loginData.tagLine
        }
    }).then(
      res => {
          if(!res.userConfirmed){
            this.confirmOTP(username);
          }
          console.log(res)
        }, err=>{
          if(err['code'] == 'UsernameExistsException'){
            this.confirmOTP(username)
          }else{
            alert(err.message)
          }
          
          console.log(err)
        }
    )
    }



    //this.router.navigate(['home']);
  } 
  /**
   * End of classs
   */
}
