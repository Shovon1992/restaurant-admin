import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { Auth } from 'aws-amplify';
import {NgRedux,select} from '@angular-redux/store';
@Component({
  selector: 'app-confirm-otp',
  templateUrl: './confirm-otp.component.html',
  styleUrls: ['./confirm-otp.component.scss']
})
export class ConfirmOTPComponent extends BaseComponent implements OnInit {
  OTP: any;
  userName;
  async ngOnInit() {
    this.ngRedux.select('userName').subscribe(
      res =>{
        this.userName = res
        console.log(res)
      }
    )
  }

  componentDidMount(){
    if(!this.userName){
      this.router.navigate(['login'])
    }
  }
  /**
   * Get the login details 
   */

  verify(){
     
     if(this.OTP){
      this.spinner.show();
      Auth.confirmSignUp(this.userName,this.OTP).then(
        res =>{
          if(res=='SUCCESS'){
            this.router.navigate([''])
          }
          console.log(res)
        }, err =>{
          console.log(err)
          alert(err.message)
        }
 
      )
      this.spinner.hide();
     } else {
       alert('Please enter vaild OTP')
     }
     
     //alert('hello')
     //this.router.navigate(['home']);
   }

   /**
    * Back to register
    */
   register(){
     this.router.navigate(['register']);
   }
  /**
   * End of class 
   */

}
