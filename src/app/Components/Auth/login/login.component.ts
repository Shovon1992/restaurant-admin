import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { Auth } from 'aws-amplify';
import { SetUserInfo } from '../../../action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit {

  username;
  password;


  ngOnInit(): void {
    localStorage.clear();
    
  }

  /**
   * Get the login details 
   */

   logIn(){
     let __this = this;
     if(this.username && this.password){
      __this.spinner.show();
      Auth.signIn(this.username,this.password).then(
        res =>{
          console.log(res.username);
          this.restaurantService.getRestaurantByCognito(res.username).subscribe(
            res =>{
              localStorage.setItem('restaurantInfo',JSON.stringify(res.data[0]))
              this.dataService.setData(res.data[0]);
              this.ngRedux.dispatch({
                type: SetUserInfo, userInfo: res.data[0]
              });
              this.router.navigateByUrl('dashboard/restaurant-dashboard');
            }, err =>{
              console.log(err);
            }
          )
          //this.confirmOTP(this.username)
        }, err =>{
          if(err.code == "UserNotConfirmedException"){
            this.confirmOTP(this.username)
          }else{
            alert(err.message)
          }
          console.log(err)
          
        }
 
      )
      this.spinner.hide();
     } else {
       alert('Please enter vaild username and password..')
     }
     
     //alert('hello')
     //this.router.navigate(['home']);
   }

   /**
    * Back to register
    */
   register(){
     this.router.navigate(['register']);
   }
  /**
   * End of class 
   */
}
