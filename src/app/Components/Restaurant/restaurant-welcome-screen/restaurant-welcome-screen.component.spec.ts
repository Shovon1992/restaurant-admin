import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantWelcomeScreenComponent } from './restaurant-welcome-screen.component';

describe('RestaurantWelcomeScreenComponent', () => {
  let component: RestaurantWelcomeScreenComponent;
  let fixture: ComponentFixture<RestaurantWelcomeScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantWelcomeScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantWelcomeScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
