import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import {Icreatwelcome } from '../../../Interface/Restaurant/icreatwelcome';
import Amplify, { Storage } from 'aws-amplify';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-restaurant-welcome-screen',
  templateUrl: './restaurant-welcome-screen.component.html',
  styleUrls: ['./restaurant-welcome-screen.component.scss']
})
export class RestaurantWelcomeScreenComponent extends BaseComponent implements OnInit {
  tempImage: any = '../../../assets/img/noImage.jpg'
  showOverlay = false
  /**
   * Variables 
   */
  restaurantData = this.dataService.getData();
  welcomeLists: any =[];
  createWelcomeData : Icreatwelcome = {
    resturantId : this.restaurantData.hash_id,
    description: '',
    image: '',
    header: '',
  }
  ngOnInit(){
    this.getWelcomeLists();
  }

  /**
   * Get resturant lists
   */
  getWelcomeLists(){
    this.spinner.show();
    this.loaddingMessage = 'Loading Restaurant lists'
    this.restaurantService.getWelcomeOfRestuatnt(this.restaurantData.hash_id).subscribe(
      res =>{
        this.spinner.hide()
        if(res.messageCode == 200){
          this.welcomeLists = res.data
        }
        
        console.log(res);

      }, err =>{
        console.log(err)
      }
    );
  }

  /**
   * Create new restaurant 
   */
  createRestaurant(){
    if(this.createWelcomeData.image && this.createWelcomeData.header && this.createWelcomeData.description ){
      this.spinner.show();
      this.loaddingMessage = 'Creating Restaurant'
      this.restaurantService.createWelcomeOfRestuatnt(this.createWelcomeData).subscribe(
        res =>{
          this.spinner.hide()
          if(res.messageCode == 200){
            console.log(document.getElementById('modalColoseBtn'));
            document.getElementById('modalColoseBtn').click();

            this.getWelcomeLists();

          }
          
          console.log(res);

        }, err =>{
          console.log(err)
        }
      );
    } else{
      alert('All Fields are mandatory..')
    }
    
  }

  /**
   * Go to restuarant dashboard page 
   */
  goRestaurantDashboard(restaurantData){
    this.dataService.setData(restaurantData);
    this.router.navigateByUrl('dashboard/restaurant-dashboard');
  }

 /**
   * Upload image to s3
   */

  uploadImage(image,imageType){
    console.log(image,imageType)
    this.reader.readAsDataURL(image[0]);
    
    this.reader.onload = () =>{
      console.log(this.reader.result);
      this.tempImage = this.reader.result;
    }
    
    //this.sanitizer.bypassSecurityTrustResourceUrl(image.target.value);
    console.log(this.tempImage);
    this.showOverlay = true;
    Storage.put('image/welcomeScreen/'+this.restaurantData.name+'/'+image[0].name,image[0],{
          bucket: environment.restaurantBUcket,
          contentType: 'image/jpg',
          progressCallback(progress) {
            console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
          },
          
    }).then (
      res => {
        console.log(res);
        this.createWelcomeData.image = 'https://'+environment.restaurantBUcket+'.s3.amazonaws.com/public/'+res['key'];
        this.createWelcomeData.image = 'https://'+environment.restaurantBUcket+'.s3.amazonaws.com/public/'+res['key'];
        this.showOverlay = false;
      }
    ).catch(err => {
      console.log(err); 
      this.showOverlay = false;
    });
    
  }
  /**
   * End of classs
   */

}
