import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { Icreaterestaurant } from 'src/app/Interface/Restaurant/icreaterestaurant';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.scss']
})
export class RestaurantListComponent extends BaseComponent implements OnInit {

  /**
   * Variables 
   */
  restaurantlists: any =[];
  createRestaurantData : Icreaterestaurant = {
    name: '',
    email: '',
    phone: '',
    tagLine: '',
    reg_no: ''
  }
  ngOnInit(){
    this.getAllRestaurant();
  }

  /**
   * Get resturant lists
   */
  getAllRestaurant(){
    this.spinner.show();
    this.loaddingMessage = 'Loading Restaurant lists'
    this.restaurantService.getAllRestuatnt().subscribe(
      res =>{
        this.spinner.hide()
        if(res.messageCode == 200){
          this.restaurantlists = res.data
        }
        
        console.log(res);

      }, err =>{
        console.log(err)
      }
    );
  }

  /**
   * Create new restaurant 
   */
  createRestaurant(){
    if(this.createRestaurantData){
      this.spinner.show();
      this.loaddingMessage = 'Creating Restaurant'
      this.restaurantService.createRestaurant(this.createRestaurantData).subscribe(
        res =>{
          this.spinner.hide()
          if(res.messageCode == 200){
            console.log(document.getElementById('modalColoseBtn'));
            document.getElementById('modalColoseBtn').click();

            this.getAllRestaurant();

          }
          
          console.log(res);

        }, err =>{
          console.log(err)
        }
      );
    } else{
      alert('All Fields are mandatory..')
    }
    
  }

  /**
   * Go to restuarant dashboard page 
   */
  goRestaurantDashboard(restaurantData){
    this.dataService.setData(restaurantData);
    this.router.navigateByUrl('dashboard/restaurant-dashboard');
  }


  /**
   * Upload image to s3 bucket 
   */

  fileUpload(image){
    console.log(image);
  //   Storage.put('test.txt', image, {
  //     progressCallback(progress) {
  //         console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
  //   },
  // });
  }
  /**
   * End of class
   */

}
