import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { ICreateTable, ITableBooking } from '../../../Interface/Restaurant/icreate-table';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-restaurant-tables',
  templateUrl: './restaurant-tables.component.html',
  styleUrls: ['./restaurant-tables.component.scss']
})
export class RestaurantTablesComponent extends BaseComponent implements OnInit {
  tempImage: any = '../../../assets/img/noImage.jpg'
  showOverlay = false
  /**
   * Variables 
   */
  restaurantData = this.dataService.getData();
  tableList: any ={};
  createtableData: ICreateTable  = {
    resturantId : this.restaurantData.hash_id,
    description: '',
    tableCode: '',
    availability: 1,
    seatCount : 2
  }
  ngOnInit(){
    this.gettableLists();
  }

  /**
   * Get resturant lists
   */
  gettableLists(){
    this.spinner.show();
    this.loaddingMessage = 'Loading Restaurant lists'
    this.restaurantService.getTableOfRestuatnt(this.restaurantData.hash_id).subscribe(
      res =>{
        this.spinner.hide()
        if(res.messageCode == 200){
            this.tableList = res.data
          
        }
        
        console.log(res);

      }, err =>{
        console.log(err)
      }
    );
  }

  /**
   * Create new restaurant 
   */
  createTable(){
    if(this.createtableData.tableCode && this.createtableData.description && this.createtableData.seatCount ){
      this.spinner.show();
      this.loaddingMessage = 'Creating Restaurant'
      this.restaurantService.createTableOfRestaurant(this.createtableData).subscribe(
        res =>{
          this.spinner.hide()
          if(res.messageCode == 200){
            console.log(document.getElementById('modalColoseBtn'));
            document.getElementById('modalColoseBtn').click();

            this.gettableLists();

          }
          
          console.log(res);

        }, err =>{
          console.log(err)
        }
      );
    } else{
      alert('All Fields are mandatory..')
    }
    
  }

  /**
   * Go to restuarant dashboard page 
   */
  goRestaurantDashboard(restaurantData){
    this.dataService.setData(restaurantData);
    this.router.navigateByUrl('dashboard/restaurant-dashboard');
  }


  /**
   * Change booking of table
   */

  freeTable(user,table) {
    let data: ITableBooking = {
      userId: user,
      tableId: table,
      bookingStatus: 3
    }

    this.spinner.show();
      this.loaddingMessage = 'Creating Restaurant'
      this.restaurantService.changeBooking(data).subscribe(
        res =>{
          this.spinner.hide()
          if(res.messageCode == 200){
            console.log(document.getElementById('modalColoseBtn'));
            document.getElementById('modalColoseBtn').click();

            this.gettableLists();

          }
          
          console.log(res);

        }, err =>{
          console.log(err)
        }
      );
    
  }

 /**
   * Upload image to s3
   */

  /**
   * End of classs
   */

}
