import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector: 'app-restaurant-dashboard',
  templateUrl: './restaurant-dashboard.component.html',
  styleUrls: ['./restaurant-dashboard.component.scss']
})
export class RestaurantDashboardComponent extends BaseComponent implements OnInit {

   restaurantData = this.dataService.getData();

  ngOnInit(): void {
    console.log(this.restaurantData)
  }



  /**
   * Go to welcome screen
   */
  goNext(path){
    this.router.navigateByUrl('dashboard/'+path)
  }
  /**
   * End of class
   */
}
