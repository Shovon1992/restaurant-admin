import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent extends BaseComponent implements OnInit {

  orderLists = [];
  ngOnInit(): void {
    this.getOrdersOfRrestaurant();
  }

 /**
   * Get orders of a restaurant 
   */
  getOrdersOfRrestaurant(){
    this.spinner.show();
    this.loaddingMessage = 'Loading Restaurant lists'
    this.oredrService.getAllOrders(this.restaurantData.hash_id).subscribe(
      res =>{
        this.spinner.hide()
        if(res.messageCode == 200){
          this.orderLists = res.data
        }
        
        console.log(res);

      }, err =>{
        console.log(err)
      }
    );
  }

  getOrderDetails(orderId){
    this.dataService.routingData = {
      orderId : orderId
    }
    this.router.navigateByUrl('dashboard/order-details/'+orderId);
  }
  /**
   * End of class
   */
}
