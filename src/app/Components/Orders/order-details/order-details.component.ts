import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { IOrderTarcking } from '../../../Interface/order/iorder-tarcking';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent extends BaseComponent implements OnInit {

  orderItems = [];
  orderId = this.activatedRoute.snapshot.paramMap.get('orderId');
  orderData : any =[];
  selectItem : any =[];
  trackingData: IOrderTarcking ={
    orderItemId : '',
    message : '',
    actionByRestaurant : this.restaurantData.hash_id,
    userMessage: '',
    status: null,
    propsedTime : 15
  };

  orederMesaage = [
    {
      'id' : -1,
      'message' : "Order has been declined by resturant"
    },
    {
      'id' : 1,
      'message' : "Order has been approved by resturant"
    },
    {
      'id' : 2,
      'message' : "Preaparing your order will be deliverd Soon"
    },
    {
      'id' : 3,
      'message' : "Your order is ready for delivary"
    },
    {
      'id' : 4,
      'message' : "Order deliverd at your table"
    }
  ]
  ngOnInit(): void {
    this.getOrderDetails();
  }

 /**
   * Get orders of a restaurant 
   */
  getOrderDetails(){
    this.spinner.show();
    this.loaddingMessage = 'Loading Restaurant lists'
    this.oredrService.getOrderDetailsById(this.orderId).subscribe(
      res =>{
        this.spinner.hide()
        if(res.messageCode == 200){
          this.orderItems = res.data
        }
        
        console.log(res);

      }, err =>{
        console.log(err)
      }
    );
  }
  openUpdateModal(item){
    console.log(item);
    this.selectItem = item;
    this.trackingData.orderItemId = item.itemId;
  }
  /**Update  tracking*/
  updateTracking(){
    console.log(this.trackingData);
    if(this.trackingData.message && this.trackingData.propsedTime ){
      this.oredrService.orderTracking(this.trackingData).subscribe(
        res =>{
          this.spinner.hide();
          console.log(res)
          if(res.messageCode == 200){
            //this.orderItems = res.data[0]
          }
          
          console.log(res);
  
        }, err =>{
          console.log(err)
        }
      );
    } else {
      alert('Please fill all data')
    }
  }

  /**
   * Chanjdfjhd
   */
  changeStatus(message){
    console.log(message)
    this.trackingData.status = this.orederMesaage[message].id;
    this.trackingData.message = this.orederMesaage[message].message;
  }
  /**
   * End of the class
   */
}
