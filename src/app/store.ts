
import { SetUserName,UpdateNotificationCount,StoreNotification, FcmToken, BrowserInfo,SetUserInfo } from './action';
import { FoodItemsComponent } from './Components/Food/food-items/food-items.component';
import { INotification } from './Interface/notification';

export interface IAppState {
    userName?: String,
    userInfo?: [],
    restaurantData?: [],
    browserInfo?: [],
    fcmToken?: String,
    notifications?: Array<INotification>,
    notificationCount: any
}

export const Initial_State: IAppState = {
    userName: null,
    userInfo: [],
    restaurantData: [],
    browserInfo: [],
    fcmToken: null,
    notifications: [],
    notificationCount : 0
};



export function rootReducer(state= Initial_State, action){
    switch(action.type){
        case SetUserName :
            console.log('from store',action.userName)
            return Object.assign({},state,{
                userName : action.userName
            })
        case BrowserInfo:
            console.log('from store --- .',action.browserInfo)
            return Object.assign({},state,{
                browserInfo : action.browserInfo
            })
        case FcmToken:
            console.log('from store --- .',action.fcmToken)
            return Object.assign(
                {}, state,{
                fcmToken : action.fcmToken
            })
        case StoreNotification:
            
            let data = {
                message : action.notifications.body,
                title: action.notifications.title,
                date: new Date()
            }
            console.log('from store --- .', action.notifications, data, typeof (state.notifications))
            // let insert_data = (state.notifications) ? state.notifications.push(data) : []
            
            // return Object.assign(
            //     {}, state,{
            //         notifications: insert_data
            // })
        
        case UpdateNotificationCount:
            // console.log('from store --- .',state,parseInt(state.notificationCount))
            return Object.assign(
                {}, state,{
                notificationCount : action.count
            })
        case SetUserInfo:
            return Object.assign(
                {}, state,{
                userInfo : action.userInfo
            })
    }
}