import { NgModule } from '@angular/core';
import { AuthGurdGuard } from './auth-gurd.guard';

import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Components/Auth/login/login.component';
import { PagenotfoundComponent } from './Components/Common/pagenotfound/pagenotfound.component';
import { HomeComponent } from './Components/Dashboard/home/home.component';
import { RegisterComponent } from './Components/Auth/register/register.component';
import { HeaderSidebarComponent } from './Components/header-sidebar/header-sidebar.component';
import { RestaurantListComponent } from './Components/Restaurant/restaurant-list/restaurant-list.component';
import { RestaurantDashboardComponent } from './Components/Restaurant/restaurant-dashboard/restaurant-dashboard.component';
import { RestaurantWelcomeScreenComponent } from './Components/Restaurant/restaurant-welcome-screen/restaurant-welcome-screen.component';
import { ConfirmOTPComponent } from './Components/Auth/confirm-otp/confirm-otp.component';
import { FoodMenuComponent } from './Components/Food/food-menu/food-menu.component';
import { FoodItemsComponent } from './Components/Food/food-items/food-items.component';
import { OrderListComponent } from './Components/Orders/order-list/order-list.component';
import { OrderDetailsComponent } from './Components/Orders/order-details/order-details.component';
import { RestaurantTablesComponent } from './Components/Restaurant/restaurant-tables/restaurant-tables.component';
const routes: Routes = [
  
  { path : 'nopage', component: PagenotfoundComponent },
  
  { path: '', component: LoginComponent },
  // { path: '', component: LoginComponent },
  // { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  {path : 'confirmOTP', component : ConfirmOTPComponent},
  { path: 'dashboard', component: HeaderSidebarComponent, children :[
    // { path : '', redirectTo : 'resturant-list', pathMatch: 'full' },
    {path: 'home', component: HomeComponent},
    {path: 'resturant-list', component: RestaurantListComponent},
    {path: 'restaurant-dashboard', component: RestaurantDashboardComponent},
    {path: 'restaurant-welcome-screen', component: RestaurantWelcomeScreenComponent},
    {path: 'food-menu', component: FoodMenuComponent},
    {path: 'food-items', component: FoodItemsComponent},
    {path: 'order-list', component: OrderListComponent},
    {path: 'order-details/:orderId', component: OrderDetailsComponent},
    {path: 'table-list', component: RestaurantTablesComponent},
  ], canActivate: [AuthGurdGuard]}
]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
