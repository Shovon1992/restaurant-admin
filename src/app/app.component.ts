import { Component } from '@angular/core';
import { FirebaseMessageService } from './Services/Notification/firebase-message.service';
import { BrowserInfo } from '../app/action';
import { NgRedux, select } from '@angular-redux/store';
import {IAppState} from '../app/store';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  message;
  constructor(
    public messagingService: FirebaseMessageService,
    public ngRedux : NgRedux<IAppState>
  ) {
    // this.messagingService.requestPermission()
    // this.messagingService.receiveMessage()
    // this.message = this.messagingService.currentMessage
  }
  title = 'restaurant-admin';


ngOnInit() {
  localStorage.clear();
  this.messagingService.requestPermission()
  this.messagingService.receiveMessage()
  // this.message = this.messagingService.currentMessage;

  // console.log(this.message )
  this.ngRedux.dispatch({
    type: BrowserInfo, browserInfo: {
    "uuid" : window.clientInformation.product+'_'+window.clientInformation.productSub,
     "device_type" : "web browser",
     "device_name" : window.clientInformation.userAgent,
  }});
}
  
}
