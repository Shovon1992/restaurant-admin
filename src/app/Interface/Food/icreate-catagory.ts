export interface ICreateCatagory {
    imageUrl ?: String,
    thumbUrl ?: String
    description?: String,
    name: String, 
    resturantId: String
}
