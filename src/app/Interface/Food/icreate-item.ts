export interface ICreateItem {
    imageUrl?: String
    thumbUrl ?: String, 
    description: String,
    name: String, 
    taste ?: String, 
    ingrediant ?: String,  
    veg : String,
    imageType: 1,
    foodCategoryId: String,
    quantity: Number,
    price: Number,
    unit: String,

}
