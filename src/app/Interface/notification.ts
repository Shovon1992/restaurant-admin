export interface INotification {
    message: String,
    title: String,
    date : Date
};