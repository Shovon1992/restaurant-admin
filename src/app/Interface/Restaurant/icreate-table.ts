export interface ICreateTable {
    mageUrl ?: String
    thumbUrl ?: String
    description: String
    tableCode: String
    tableType ?: String
    seatCount: Number,
    availability: Number,
    resturantId: String
}


export interface ITableBooking {
    tableId: string,
    userId: string,
    bookingStatus: number
}