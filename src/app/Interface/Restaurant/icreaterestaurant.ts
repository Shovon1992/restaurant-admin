export interface Icreaterestaurant {
    tagLine: String,
    name: String,
    email :  String,
    phone : String,
    image?: String,
    reg_no: String,
}
