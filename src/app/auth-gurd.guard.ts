import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthGurdService } from './Services/auth-gurd.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGurdGuard implements CanActivate {
  constructor(private authGurdService: AuthGurdService) { }
  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }
  
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log(this.authGurdService.isAuthenticate)
    if (this.authGurdService.auth()) {
      return true;
    } else {
      return false;
    }
  }
}
