export const SignUp = 'SignUp';
export const SignIn = 'SignIn';
export const SetUserName = 'SetUserName';
export const BrowserInfo = 'BrowserInfo';
export const FcmToken = 'FcmToken';
export const StoreNotification = 'StoreNotification';
export const UpdateNotificationCount = 'UpdateNotificationCount';
export const SetUserInfo = "SetUserInfo";