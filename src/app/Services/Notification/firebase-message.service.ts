import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs'
import { FcmToken, StoreNotification } from '../../action';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../store';

@Injectable()
@Injectable({
  providedIn: 'root'
})
export class FirebaseMessageService {
	currentMessage = new BehaviorSubject(null);
	constructor(
		private angularFireMessaging: AngularFireMessaging,
		public ngRedux : NgRedux<IAppState>
	) {
		// this.angularFireMessaging.messaging.subscribe(
		// (_messaging) => {
		// _messaging.onMessage = _messaging.onMessage.bind(_messaging);
		// _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
		// }
		// )
	}
	requestPermission() {
		this.angularFireMessaging.requestToken.subscribe(
			(token) => {
				// console.log(token);
				
				//  select('notificationCount').subscribe(
				// 	res=> console.log(res)
				// )
				
					
				console.log("new message received. ", "payload" );
				this.ngRedux.dispatch({
					type: FcmToken, fcmToken: token
				});
			},
			(err) => {
				console.error('Unable to get permission to notify.', err);
			});
	}
	receiveMessage() {
		this.angularFireMessaging.messages.subscribe(
			(payload) => {
				console.log("new message received. ", payload );
				// this.ngRedux.dispatch({
				// 	type: UpdateNotificationCount,
				// 	count : (this.ngRedux.getState().notificationCount) ? this.ngRedux.getState().notificationCount + 1 : 1
				// });
				
				this.ngRedux.dispatch({
					type: StoreNotification, notifications: payload['notification']
				});
				this.currentMessage.next(payload);
			})
	}
}