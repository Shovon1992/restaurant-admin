import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';
import { Icreaterestaurant } from 'src/app/Interface/Restaurant/icreaterestaurant';
import { Icreatwelcome } from 'src/app/Interface/Restaurant/icreatwelcome';
import { ICreateTable, ITableBooking } from 'src/app/Interface/Restaurant/icreate-table';
import {NgRedux,select} from '@angular-redux/store';
import { IAppState } from '../../store';

@Injectable({
  providedIn: 'root'
})
export class ResturantService extends RootService {

  constructor(http: HttpClient,
      ngRedux : NgRedux<IAppState>
  ) {
    super(http,ngRedux);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }
  /**
   * Get all resturant list 
  */
    getAllRestuatnt(): Observable<any>{
      return this.http.get(this.restaurant,this.httpOptions).pipe(
          timeout(this.timeOutLimit)
        );
    }
  /**
   * Create new restauturant 
   */
  createRestaurant(data: Icreaterestaurant): Observable<any>{
    return this.http.post(this.restaurant,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }


  /**
   * Get all resturant list 
  */
  getWelcomeOfRestuatnt(id): Observable<any>{
    this.user = this.ngRedux.getState().userInfo
    return this.http.get(this.restaurantWelcomeScreen+'/'+this.user['hash_id'],this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }

  /**
   * Create new restauturant 
   */
  createWelcomeOfRestuatnt(data: Icreatwelcome): Observable<any>{
    return this.http.post(this.restaurantWelcomeScreen,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }


  /**
   * get restaurant details by cognito id
   */
  getRestaurantByCognito(id): Observable<any>{
    
    let data = 
      {
    "resturant_id" : id,
     "uuid" : this.ngRedux.getState().browserInfo['uuid'],
     "device_type" : this.ngRedux.getState().browserInfo['device_type'],
     "device_name" : this.ngRedux.getState().browserInfo['device_name'],
     "token" :  this.ngRedux.getState().fcmToken
    }
    // console.log(data)
    return this.http.post(this.advanceLogin,data,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }
 
  /**
   * Get all table lists of a perticular restaurant 
  */
  getTableOfRestuatnt(id): Observable<any>{
    this.user = this.ngRedux.getState().userInfo
    return this.http.get(this.restaurantTables+'/'+this.user['hash_id'],this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }

  /**
   * Create new table of a restauturant 
   */
  createTableOfRestaurant(data: ICreateTable): Observable<any>{
    return this.http.post(this.restaurantTables,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }

  /**
   * Change booking status
   */
  changeBooking(data : ITableBooking):Observable<any>{
    return this.http.post(this.booking,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }
  /**
   * End of class
   */
}
