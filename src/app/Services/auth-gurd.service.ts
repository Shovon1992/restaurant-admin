import { Injectable } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from 'src/app/store';


@Injectable({
  providedIn: 'root'
})
export class AuthGurdService {

  constructor( public ngRedux : NgRedux<IAppState>){
  }
  isAuthenticate = false;
  
  auth(): boolean {
    let user = this.ngRedux.getState().userInfo
    // console.log(user)
    if (user) {
      this.isAuthenticate = true;
      return this.isAuthenticate;
    }
    this.isAuthenticate = false;
    return this.isAuthenticate;
  }
}
