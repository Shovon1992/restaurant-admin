import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';
import { ICreateCatagory } from '../../Interface/Food/icreate-catagory';
import { ICreateItem } from 'src/app/Interface/Food/icreate-item';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from 'src/app/store';


@Injectable({
  providedIn: 'root'
})
export class FoodService extends RootService {

  constructor(http: HttpClient, public ngRedux : NgRedux<IAppState>){
    super(http,ngRedux);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }
  /**
   * Get all resturant list 
  */
  getAllFoodMenues(resturantId): Observable<any>{
      this.user = this.ngRedux.getState().userInfo
      return this.http.get(this.foodCatagoryList+'/'+this.user['hash_id'],this.httpOptions).pipe(
          timeout(this.timeOutLimit)
        );
    }


    /**
     * Create food menue catagory
     */
    createFoodMenues(data: ICreateCatagory ): Observable<any>{
      return this.http.post(this.foodCatagoryList,data,this.httpOptions).pipe(
          timeout(this.timeOutLimit)
        );
    }

    /**
     * Create food items
     */
    createFoodItems(data: ICreateItem ): Observable<any>{
      return this.http.post(this.foodItem,data,this.httpOptions).pipe(
          timeout(this.timeOutLimit)
        );
    }

    /**
   * Get all resturant list 
  */
 getAllFoodItemsOfRestaurant(resturantId): Observable<any>{
  return this.http.get(this.foodItemOfResturant+"/"+resturantId,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
}


    /**
     * End of the class
     */
}
