import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';
import { IOrderTarcking } from 'src/app/Interface/order/iorder-tarcking';
import { FcmToken, StoreNotification } from '../../action';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from 'src/app/store';


@Injectable({
  providedIn: 'root'
})
export class OrderService extends RootService {

  constructor(http: HttpClient, public ngRedux : NgRedux<IAppState>){
    super(http,ngRedux);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }
 
  /**
   * Get all resturant list 
  */
  getAllOrders(resturantId): Observable<any>{
    this.user = this.ngRedux.getState().userInfo
      return this.http.get(this.orderList+'/'+this.user['hash_id'],this.httpOptions).pipe(
          timeout(this.timeOutLimit)
        );
    } 
/**
   * Get all resturant list 
  */
 getOrderDetailsById(orderId): Observable<any>{
  return this.http.get(this.orderDetails+'/'+orderId,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  } 

/**
   * Uupdate order  taracking
  */
 orderTracking(data : IOrderTarcking): Observable<any>{
  return this.http.post(this.updateTracking,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  } 



    
    /**
     * End of the class
     * 
     */
}