import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import {NgRedux,select} from '@angular-redux/store';
import { IAppState } from '../store';

@Injectable({
  providedIn: 'root'
})
export class RootService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  httpOptions = {
    headers: this.headers
  };
  user: any;
  constructor(
    public http: HttpClient,
    public ngRedux : NgRedux<IAppState>
  ) { 
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+ localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
    
  }
  timeOutLimit = 1000*15; 
  
  /**
   * Base url for api call
   */
  base_url = environment.baseUrl;

  /**
   * Functions names
   */
  restaurant = this.base_url +'restaurant';
  restaurantWelcomeScreen = this.base_url+'resturant/welcome';
  restaurantByCognito = this.base_url+'get/resturant';
  restaurantTables = this.base_url+'resturant/tables';
  
  foodCatagoryList = this.base_url+'resturant/food/catagory';
  foodItem = this.base_url+'resturant/food/item';
  updateTracking = this.base_url+'orderTracking';
  foodItemOfResturant = this.base_url+'resturant/food/item/search';
  orderList = this.base_url+'resturant/order';
  orderDetails = this.base_url+'order/details';

  // orderByRestaurant = bas
  /**
   * Base url for food menues 
   */
  /** get resturant details  */
  advanceLogin = this.base_url + 'get/resturant';
  booking = this.base_url + 'resturant/table/booking';
  /**
   * End
   */
}
