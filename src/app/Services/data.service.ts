import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  routingData: any;
  constructor() { }
  /**
   * Setting data 
   */
  setData(data){ 
    localStorage.setItem('data', JSON.stringify(data));
   }
   getData(){
      return JSON.parse(localStorage.getItem('data'));
   }
  /**
   * End of the class 
   */
}
