import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/Auth/login/login.component';
import { RegisterComponent } from './Components/Auth/register/register.component';
import { PagenotfoundComponent } from './Components/Common/pagenotfound/pagenotfound.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from '@angular/forms';
import {HeaderSidebarModule} from './Components/header-sidebar/header-sidebar.module';
import { RestaurantListComponent } from './Components/Restaurant/restaurant-list/restaurant-list.component';
import { RestaurantDashboardComponent } from './Components/Restaurant/restaurant-dashboard/restaurant-dashboard.component';
import { RestaurantWelcomeScreenComponent } from './Components/Restaurant/restaurant-welcome-screen/restaurant-welcome-screen.component';
import { AmplifyUIAngularModule } from '@aws-amplify/ui-angular';
import { ConfirmOTPComponent } from './Components/Auth/confirm-otp/confirm-otp.component';
import {NgRedux, NgReduxModule} from '@angular-redux/store';
import {IAppState, rootReducer, Initial_State} from './store';
import { FoodMenuComponent } from './Components/Food/food-menu/food-menu.component';
import { FoodItemsComponent } from './Components/Food/food-items/food-items.component';
import { OrderListComponent } from './Components/Orders/order-list/order-list.component';
import { OrderDetailsComponent } from './Components/Orders/order-details/order-details.component';
import { RestaurantTablesComponent } from './Components/Restaurant/restaurant-tables/restaurant-tables.component';
import { FirebaseMessageService } from './Services/Notification/firebase-message.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    PagenotfoundComponent,
    RestaurantListComponent,
    RestaurantDashboardComponent,
    RestaurantWelcomeScreenComponent,
    ConfirmOTPComponent,
    FoodMenuComponent,
    FoodItemsComponent,
    OrderListComponent,
    OrderDetailsComponent,
    RestaurantTablesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    FormsModule,
    BrowserAnimationsModule,
    HeaderSidebarModule,
    AmplifyUIAngularModule,
    NgReduxModule,
    AngularFireDatabaseModule,
      AngularFireAuthModule,
      AngularFireMessagingModule,
      AngularFireModule.initializeApp(environment.firebase),
  ],
  providers: [
    FirebaseMessageService,
    AngularFirestore,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public ngRedux: NgRedux<IAppState>){
    ngRedux.configureStore(rootReducer,Initial_State);
  }
 }
