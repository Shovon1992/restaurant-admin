export const environment = {
  production: true,
  baseUrl : 'https://ydc737ldu2.execute-api.us-east-1.amazonaws.com/dev/api/v1/',
  baseUrlFood : 'https://trzbi0eey7.execute-api.us-east-1.amazonaws.com/dev/api/v1/',
  baseUrlResturantOrder: 'https://hfkwxrsz67.execute-api.us-east-1.amazonaws.com/dev/api/v1/orders',
  baseUrlOrder : 'https://hfkwxrsz67.execute-api.us-east-1.amazonaws.com/dev/api/v1/',
  restaurantBUcket: 'menume-dev-restaurant-doc',
  
  catagory : 'menume-dev-food-category-doc',
  catagoryThumb : 'menume-dev-food-category-thumb-doc',

  foodItem : 'menume-dev-food-item-doc',
  foodItemThumb : 'menume-dev-food-item-thumb-doc'
};
