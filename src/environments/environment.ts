// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl : 'https://cwwgzezy54.execute-api.us-east-1.amazonaws.com/dev/api/dev/',
  // baseUrlFood : 'https://trzbi0eey7.execute-api.us-east-1.amazonaws.com/dev/api/v1/',
  // baseUrlResturantOrder: 'https://hfkwxrsz67.execute-api.us-east-1.amazonaws.com/dev/api/v1/orders',
  // baseUrlOrder : 'https://hfkwxrsz67.execute-api.us-east-1.amazonaws.com/dev/api/v1/',
  restaurantBUcket: 'restaurant-doc',
  
  catagory : 'resturant-food-category-doc',

  foodItem : 'resturant-food-item-doc',
  firebase: {
    apiKey: "AIzaSyABHh5Cxk8RprtW5V1GJGHrZ7oQlEKVLzE",
    authDomain: "restaurant-management-52482.firebaseapp.com",
    databaseURL: "https://restaurant-management-52482.firebaseio.com",
    projectId: "restaurant-management-52482",
    storageBucket: "restaurant-management-52482.appspot.com",
    messagingSenderId: "201714605537",
    appId: "1:201714605537:web:e17fb8263db8cdf5edc6a0",
    measurementId: "G-MC3XBDC25Q"
  },
  firebase_server : 'AAAALvcgneE:APA91bF7dccKArVx5sHc_QXtoCFRXWcwMDBas6NEwi9Xu2D3ss5AWLIdvCoWAmrIIqqusQ5g8zAeFtyzc1WWPliaIoXbMtHBGItegxgelIRcDs7m-3kW3RedGSTWXMHPAgCJLTn8WHdc'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
