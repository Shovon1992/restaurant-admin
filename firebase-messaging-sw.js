// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries

import { environment } from "./src/environments/environment";

// are not available in the service worker.
importScripts("https://www.gstatic.com/firebasejs/6.1.4/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/6.1.4/firebase-messaging.js");

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
// firebase.initializeApp({
//   messagingSenderId: environment.firebase_server,
// });

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log("BackgroundMessageHandler registered");
  const notificationTitle = payload.title;
  const notificationOptions = {
    body: payload.body,
    icon: "alarm.png",
  };

  return self.registration.showNotification("Nuevo pedido", {
    body: "Se ha registrado un nuevo pedido",
  });
});

workbox.core.setCacheNameDetails({ prefix: "chatbot-dashboard" });
self.addEventListener("message", (event) => {
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});
